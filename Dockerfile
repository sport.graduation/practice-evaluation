FROM openjdk:11.0.16-oracle

WORKDIR /app

EXPOSE 9500

COPY target/*.jar evaluation.jar

ENTRYPOINT [ "java","-jar" ,"./evaluation.jar"]