package com.example.evaluation.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class AddSRPE {
    private long playerID;
    private long teamID;
    private long startTimeMillis;
    private long endTimeMillis;
    private int rating;

}

