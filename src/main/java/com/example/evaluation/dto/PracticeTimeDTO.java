package com.example.evaluation.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PracticeTimeDTO {
    private long startTimeMillis;
    private long endTimeMillis;

}
