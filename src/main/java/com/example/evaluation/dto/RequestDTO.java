package com.example.evaluation.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;


@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class RequestDTO implements Serializable {
    private long playerID;
    private long weekStartMillis;
    private long weekEndMillis;

}
