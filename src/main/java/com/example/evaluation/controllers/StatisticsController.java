package com.example.evaluation.controllers;

import com.example.evaluation.dto.RequestDTO;
import com.example.evaluation.model.PracticesLoadInfo;
import com.example.evaluation.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/evaluation")
public class StatisticsController {
    @Autowired
    StatisticsService statisticsService;


    @PostMapping(value = "/stats")
    public PracticesLoadInfo showStatistics(@RequestBody RequestDTO request) {
        PracticesLoadInfo info = statisticsService.doStatistics(request);
        return info;
    }

/*    @GetMapping(value = "/all")
    public ResponseEntity<PracticeTimeDTO[]> all() {

        return statisticsService.getPracticeTime(5L);
    }
    @GetMapping(value = "/a")
    public String a() {
            statisticsService.ad();
        return "Done";
    }*/
}
