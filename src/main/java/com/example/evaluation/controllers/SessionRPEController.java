package com.example.evaluation.controllers;

import com.example.evaluation.dto.AddSRPE;
import com.example.evaluation.model.db.SessionRPE;
import com.example.evaluation.services.SessionRPEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/evaluation")
public class SessionRPEController {

    @Autowired
    SessionRPEService sessionRPEService;

    @PostMapping(value = "/add")
    public String addSessionRPE(@RequestBody AddSRPE request) {
        sessionRPEService.addSessionRatingOfPerceivedExertion(request);
        return "Session RPE has been added";
    }
}
