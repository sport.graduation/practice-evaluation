package com.example.evaluation.services;


import com.example.evaluation.dto.RequestDTO;
import com.example.evaluation.model.PracticesLoadInfo;
import com.example.evaluation.model.db.SessionRPE;
import com.example.evaluation.model.db.SessionRPERepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class StatisticsService {

    @Autowired
    SessionRPERepository repo;

    /*
    public double calculateStrainForWeek(int acuteLoad,double monotony){
        double strain ;
        strain = monotony * acuteLoad;
        System.out.println("Strain = " + strain);
        return strain;
    }
*/
    public int calculateDailyTrainingLoad(long playerID, long dayStart, long dayEnd) {
        int dailyLoad = 0;
        List<SessionRPE> list = returnSRPEDuringTime(playerID, dayStart, dayEnd);
        for (SessionRPE s : list) {
            dailyLoad += s.getRating() * ((s.getEndTimeMillis() - s.getStartTimeMillis()) / 60000);
//            System.out.println("Daily Load = " + dailyLoad + "  " + s.getOtherNotes());
        }
        return dailyLoad;
    }

    public List<SessionRPE> returnSRPEDuringTime(long playerID, long start, long end) {
        return repo.findByPlayerIDAndStartTimeMillisGreaterThanEqualAndEndTimeMillisLessThanEqual(playerID, start, end);
    }

    public List<Integer> returnListOfDailyLoadsForWeek(long playerID, long weekStart, long weekEnd) {
        List<Integer> dailyLoads = new ArrayList<>();
        do {
            dailyLoads.add(calculateDailyTrainingLoad(playerID, weekStart, weekStart + 86400000));
//            System.out.println("Daily Load = " + dailyLoads.get(dailyLoads.size() - 1));
            weekStart += 86400000L;
        } while (weekEnd > weekStart);
        return dailyLoads;
    }

    public double calculateMeanForWeek(List<Integer> dailyLoads) {
        double mean = 0;
        for (Integer i : dailyLoads) {
            mean += i;
        }
        mean /= 7;
//        System.out.println("mean = " + mean);
        return mean;
    }

    public int calculateAcuteLoad(List<Integer> weekLoads) {
        int acuteLoad = 0;
        for (Integer i : weekLoads) {
            acuteLoad += i;
        }
//        System.out.println("Acute Load = "+ acuteLoad);
        return acuteLoad;
    } // Acute Load is the sum of training load for a week

    public double calculateStandardDeviationForWeek(List<Integer> dailyLoads, double mean) {
        double sd = 0;
        for (Integer i : dailyLoads) {
            sd = sd + Math.pow((i - mean), 2);
        }
        sd = Math.sqrt((sd / 7));
//        System.out.println("Standard Deviation = " + sd);
        return sd;
    }

    public double calculateMonotonyForWeek(List<Integer> weekLoads) {
        double mean = calculateMeanForWeek(weekLoads);
        double sd = calculateStandardDeviationForWeek(weekLoads, mean);
        double monotony = mean / sd;
//        System.out.println("monotony = " + monotony);

        return monotony;
    }

    public int calculateChronicLoad(List<Integer> fourWeeks) {
        int chronicLoad = 0;

        for (Integer i : fourWeeks) {
            chronicLoad += i;
        }
        chronicLoad /= 4;
        //System.out.println("chronic Load = "+ chronicLoad);
        return chronicLoad;
    }

    public List<Integer> returnListOf4WeeksAcuteLoad(long playerID, long timeStart, long timeEnd) {
        List<Integer> fourWeeks = new ArrayList<>();
        long stop = timeEnd;
        timeStart = timeStart - 1814400000; // back in time for 3 weeks
        timeEnd = timeEnd - 1814400000; // back in time for 3 weeks
        while (timeStart < stop) {
            fourWeeks.add(calculateAcuteLoad(returnListOfDailyLoadsForWeek(playerID, timeStart, timeEnd)));
            timeStart += 604800000; // advance one week
            timeEnd += 604800000; // advance one week
        }
        return fourWeeks;
    }

    public double calculateACWR(int currentAcuteLoad, int chronicLoad) {
        return (double) currentAcuteLoad / chronicLoad;
    }

    public List<Double> calculateMeasurementsForNextWeek(List<Integer> fourWeeks, double ACWR) {
        List<Double> temp = new ArrayList<>();
        double newACWR = 0;
        int newAcuteLoad = 0;
        double nextWeekLoadIncrease = 0;
        int sumOfThreeWeeks = 0; // this will be current weeks + last 2 weeks
        for (int i = 1; i < fourWeeks.size(); i++)
            sumOfThreeWeeks += fourWeeks.get(i);

        if (ACWR > 1.3) {
            newACWR = ACWR / 4;
            newAcuteLoad = (int) ((newACWR * (sumOfThreeWeeks)) / (4 - newACWR));
            nextWeekLoadIncrease = (double) 100 * (newAcuteLoad - fourWeeks.get(fourWeeks.size() - 1)) / fourWeeks.get(fourWeeks.size() - 1);
        } else if (ACWR < 1.3) {
            nextWeekLoadIncrease = 15;
            newAcuteLoad = (int) (fourWeeks.get(fourWeeks.size() - 1) + (0.15 * fourWeeks.get(fourWeeks.size() - 1)));
            newACWR = (double) (4 * newAcuteLoad) / (newAcuteLoad + sumOfThreeWeeks);

        }

        temp.add(newACWR);
        temp.add(nextWeekLoadIncrease);
        return temp;

    }

    public PracticesLoadInfo doStatistics(RequestDTO request) {
        List<Integer> currentWeekLoads = returnListOfDailyLoadsForWeek(request.getPlayerID(), request.getWeekStartMillis(), request.getWeekEndMillis());
        List<Integer> previousWeekLoads = returnListOfDailyLoadsForWeek(request.getPlayerID(), request.getWeekStartMillis() - 604800000, request.getWeekEndMillis() - 604800000);
        List<Integer> fourWeeksLoads = returnListOf4WeeksAcuteLoad(request.getPlayerID(), request.getWeekStartMillis(), request.getWeekEndMillis());
        int currentWeekAcuteLoad = calculateAcuteLoad(currentWeekLoads);
        int previousWeekAcuteLoad = calculateAcuteLoad(previousWeekLoads);
        double monotony = calculateMonotonyForWeek(currentWeekLoads);
        int chronicLoad = calculateChronicLoad(fourWeeksLoads);
        double ACWR = calculateACWR(currentWeekAcuteLoad, chronicLoad);
        double loadIncrease = (double) 100 * (currentWeekAcuteLoad - previousWeekAcuteLoad) / previousWeekAcuteLoad;
        List<Double> nextWeekMeasurements = calculateMeasurementsForNextWeek(fourWeeksLoads, ACWR);

        PracticesLoadInfo info = new PracticesLoadInfo();
        info.setPlayerID(request.getPlayerID());
        info.setCurrWeekACWR(ACWR);
        info.setNextWeekACWR(nextWeekMeasurements.get(0));
        info.setCurrWeekLoadIncrease(loadIncrease);
        info.setNextWeekLoadIncrease(nextWeekMeasurements.get(1));
        info.setCurrWeekMonotony(monotony);
        return info;
    }


//
//    public int calculateDailyTrainingLoad(long playerID, long dayStart, long dayEnd) {
//        int dailyLoad = 0;
//        List<SessionRPE> list = returnSRPEDuringTime(playerID, dayStart, dayEnd);
//        for (SessionRPE s : list) {
//            dailyLoad += s.getRating() * ((s.getEndTimeMillis() - s.getStartTimeMillis()) / 60000);
//            System.out.println("Daily Load = " + dailyLoad + "  " + s.getOtherNotes());
//        }
//        return dailyLoad;
//    }
//    public List<SessionRPE> returnSRPEDuringTime(long playerID, long start, long end) {
//        return repo.findByPlayerIDAndStartTimeMillisGreaterThanEqualAndEndTimeMillisLessThanEqual(playerID, start, end);
//    }
//    public List<Integer> returnListOfDailyLoadsForWeek(long playerID, long weekStart, long weekEnd){
//        List<Integer> dailyLoads = new ArrayList<>();
//        do {
//            dailyLoads.add(calculateDailyTrainingLoad(playerID, weekStart, weekStart + 86400000));
//            System.out.println("Daily Load = " + dailyLoads.get(dailyLoads.size() - 1));
//            weekStart += 86400000L;
//        } while (weekEnd > weekStart);
//        return dailyLoads;
//    }
//    public double calculateMeanForWeek(List<Integer> dailyLoads) {
//        double mean = 0;
//        for (Integer i : dailyLoads) {
//            mean += i;
//        }
//        mean /= 7;
//        System.out.println("mean = " + mean);
//        return mean;
//    }
//    public int calculateAcuteLoad(List<Integer> weekLoads) {
//        int acuteLoad = 0;
//        for (Integer i : weekLoads){
//            acuteLoad+= i;
//        }
//        System.out.println("Acute Load = "+ acuteLoad);
//        return acuteLoad;
//    } // Acute Load is the sum of training load for a week
//    public double calculateStandardDeviationForWeek(List<Integer> dailyLoads, double mean) {
//        double sd = 0;
//        for (Integer i : dailyLoads) {
//            sd = sd + Math.pow((i - mean), 2);
//        }
//        sd = Math.sqrt((sd / 7));
//        System.out.println("Standard Deviation = " + sd);
//        return sd;
//    }
//    public double calculateMonotonyForWeek(long playerID, long weekStart, long weekEnd) {
//        List<Integer> dailyLoads = returnListOfDailyLoadsForWeek(playerID, weekStart, weekEnd);
//        double mean = calculateMeanForWeek(dailyLoads);
//        double sd = calculateStandardDeviationForWeek(dailyLoads,mean);
//        double monotony = mean / sd;
//        System.out.println("monotony = " + monotony);
//
//        return monotony;
//    }
//    public double calculateStrainForWeek(long playerID, long weekStart, long weekEnd){
//        double strain ;
//        double monotony = calculateMonotonyForWeek(playerID, weekStart, weekEnd);
//        double weeklyTL = calculateAcuteLoad(playerID, weekStart, weekEnd);
//        strain = monotony * weeklyTL;
//        System.out.println("Strain = " + strain);
//        return strain;
//    }
//
//   public int calculateAcuteLoad(long playerID, long weekStart, long weekEnd) {
//        int acuteLoad = 0;
//        List<Integer> dailyLoads = returnListOfDailyLoadsForWeek(playerID, weekStart, weekEnd);
//        for (Integer i : dailyLoads){
//            acuteLoad+= i;
//        }
//        System.out.println("Acute Load = "+ acuteLoad);
//        return acuteLoad;
//    } // Acute Load is the sum of training load for a week*/

/*public ResponseEntity<PracticeTimeDTO[]> getPracticeTime(long teamID){
        String url = String.format("http://localhost:9100/api/v1/practice/all/%o",teamID);
        return restTemplate.getForEntity(url,PracticeTimeDTO[].class);
    }
    public void ad(){
        List<PracticeTimeDTO> practiceTimeDTOList= List.of(getPracticeTime(5L).getBody());
        Random random = new Random();
        for (PracticeTimeDTO p:practiceTimeDTOList) {
            SessionRPE sessionRPE =new SessionRPE();
            sessionRPE.setEndTimeMillis(p.getEndTimeMillis());
            sessionRPE.setStartTimeMillis(p.getStartTimeMillis());
            sessionRPE.setRating(random.nextInt(9));
            sessionRPE.setPlayerID(9);
            sessionRPE.setTeamID(5);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(p.getStartTimeMillis());
            sessionRPE.setOtherNotes((calendar.get(Calendar.DAY_OF_MONTH)) +"/"+ Integer.valueOf(calendar.get(Calendar.MONTH)+1));
            repo.save(sessionRPE);
        }
    }*/
}
