package com.example.evaluation.services;

import com.example.evaluation.dto.AddSRPE;
import com.example.evaluation.model.db.SessionRPE;
import com.example.evaluation.model.db.SessionRPERepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SessionRPEService {

    private Logger log = LoggerFactory.getLogger(SessionRPEService.class);

    @Autowired
    SessionRPERepository repo;

    public void addSessionRatingOfPerceivedExertion(AddSRPE request){
        SessionRPE sessionRPE =new SessionRPE();
        sessionRPE.setRating(request.getRating());
        sessionRPE.setPlayerID(request.getPlayerID());
        sessionRPE.setTeamID(request.getTeamID());
        sessionRPE.setStartTimeMillis(request.getStartTimeMillis());
        sessionRPE.setEndTimeMillis(request.getEndTimeMillis());
        repo.save(sessionRPE);
        log.info("Session RPE added Successfully");
    }

}
