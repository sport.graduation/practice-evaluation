package com.example.evaluation.model;


import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor

public class PracticesLoadInfo implements Serializable {
    private long playerID;
    private double currWeekACWR;
    private double nextWeekACWR;
    private double currWeekLoadIncrease;
    private double nextWeekLoadIncrease;
    private double currWeekMonotony;
}
