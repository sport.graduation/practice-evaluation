package com.example.evaluation.model.db;


import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "srpe")
@Entity
public class SessionRPE implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "srpe")
    @SequenceGenerator(name="srpe", sequenceName = "srpe_seq")
    private long id;
    private long playerID;
    private long teamID;
    private long startTimeMillis;
    private long endTimeMillis;
    private int rating;
    private String previousSuffer;
    private String practiceOpinions;
    private String otherNotes;
}
