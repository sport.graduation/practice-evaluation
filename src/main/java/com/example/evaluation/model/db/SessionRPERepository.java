package com.example.evaluation.model.db;

import com.example.evaluation.model.db.SessionRPE;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SessionRPERepository extends JpaRepository<SessionRPE, Long> {
    List<SessionRPE> findByPlayerIDAndStartTimeMillisGreaterThanEqualAndEndTimeMillisLessThanEqual(long playerID,long start, long end);
}
